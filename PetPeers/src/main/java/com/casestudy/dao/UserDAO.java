package com.casestudy.dao;


import com.casestudy.model.User;

/**
 * 
 * @author KAJAL RANI
 *
 */

public interface UserDAO {
	
	public abstract User saveUser(User user);
	
	public abstract User authenticateUser(User user);
	
	public abstract User findByUsername(String username);

}
