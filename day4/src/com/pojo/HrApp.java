package com.pojo;

public class HrApp {

	public static void main(String[] args) {
		Employee emp1 = new Employee();
		emp1.setEmpId(12);
		emp1.setEmpName("Kajal");

		Address address = new Address();
		address.setDoorNo(002);
		address.setCity("Meerut");
		address.setState("UP");

		emp1.setVar(address);

		System.out.println("Employee ID : " + emp1.getEmpId());
		System.out.println("Employee Name : " + emp1.getEmpName());

		Address add = emp1.getVar();

		System.out.println("Employee Door no : " + emp1.getVar().getDoorNo());
		System.out.println("Employee City : " + add.getState());
		System.out.println("Employee State : " + add.getState());

	}

}
