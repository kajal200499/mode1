<!-- @author Chandni -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
	
    <%@ page errorPage="error.jsp" %> 
	
<%@page import="com.casestudy.model.User"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPEhtml>
<html>
<head>
<metacharset="ISO-8859-1">
<title>Registration Page</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

<style>
.error {
	color: #ff0000;
}
</style>
</head>
<body>

	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Pet Shop</a>
			</div>

			<ul class="nav navbar-nav navbar-right">
				<li><a href="login"><span
						class="glyphicon glyphicon-log-in"></span> Login</a></li>
			</ul>
		</div>
	</nav>

	<div class="row">
		<div class="container">
			<div>
				<h1>Register</h1>
			</div>
		</div>
	</div>

	<form:form action="register" modelAttribute="user" method="POST">
		<div class="row">
			<div class="container">
				<div>
					<label for="userName">Name:</label> <br>
					<form:input path="userName" name="username" type="text"
						class="form-control" />
					<form:errors path="userName" class="error">
					</form:errors>
					<br> <label for="userPassword">Password:</label> <br>
					<form:input path="userPassword" name="userPassword" type="text"
						class="form-control" />
					<form:errors path="userPassword" class="error">
					</form:errors>
					<br> <br> <label for="confirmPassword">Confirm Password: </label> <br>
					<form:input path="confirmPassword" name="confirmPassword"
						type="password" class="form-control" />
					<form:errors path="confirmPassword" class="error">
					</form:errors>
					<br> <input type="submit" class="btn btn-primary"
						value="Register" />
				</div>
			</div>
		</div>

	</form:form>


</body>
</html>






