package com.main;

import com.pojo.Student;
/**
 * The demo is for constructor
 * @author KAJAL RANI
 *
 */

public class BasicDatatypeConstructor {
	
	public static void main(String[] args) {
		Student student1 = new Student();
		
		student1.setStudNo(10);
		student1.setStudentName("Heloo");
		student1.setStudentAge(23);
		
		System.out.println("Student 1 Id : " + student1.getStudNo());
		System.out.println("Student 1 Name : " + student1.getStudentName());
		System.out.println("Student 1 Age : " + student1.getStudentAge());
		
		Student student2 = new Student(11, "HII", 25);
		
		System.out.println("\nStudent 2 Id : " + student2.getStudNo());
		System.out.println("Student 2 Name : " + student2.getStudentName());
		System.out.println("Student 2 Age : " + student2.getStudentAge());
		
		Student student3 = new Student(student2);
		
		System.out.println("\nStudent 3 Id : " + student3.getStudNo());
		System.out.println("Student 3 Name : " + student3.getStudentName());
		System.out.println("Student 3 Age : " + student3.getStudentAge());
		
		
		student1 = null;
		student2 = null;
		student3 = null;
		
	}
}
