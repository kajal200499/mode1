package com.casestudy.test;


/**
 * 
 * @author KAJAL RANI
 *
 */


import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.casestudy.dao.PetDAO;
import com.casestudy.dao.UserDAO;
import com.casestudy.model.Pet;
import com.casestudy.model.User;


@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/appConfig-servlet.xml" })
@RunWith(SpringRunner.class)
public class PetDAOTest {
	
	@Autowired
	PetDAO petDAO;
	
	@Autowired
	UserDAO userDAO;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testTestGetAllPets() {
		

		Pet pet = new Pet();
		pet.setPetName("Happy");
		pet.setPetAge(1);
		pet.setPetPlace("happyVilla");
		
		Pet pet1 = new Pet();

		pet1.setPetName("Sweety");
		pet1.setPetAge(2);
		pet1.setPetPlace("SweetyVilla");

		petDAO.savePet(pet);

		petDAO.savePet(pet1);

		List<Pet> listOfAllPets = petDAO.getAllPets();

		Assert.assertEquals(pet.getPetName(), listOfAllPets.get(0).getPetName());
		Assert.assertEquals(pet1.getPetName(), listOfAllPets.get(1).getPetName());
		
		pet = null;
		pet1 = null;
		listOfAllPets = null;
	}

	@Test
	public void testTestGetMyPets() {
		User user = new User();
		user.setUserName("Test2");
		user.setUserPassword("test22");

		User savedUser = userDAO.saveUser(user);

		Pet pet = new Pet();
		pet.setPetName("Happy");
		pet.setPetAge(1);
		pet.setPetPlace("happyVilla");
		pet.setUser(user);

		Pet pet1 = new Pet();

		pet1.setPetName("Sweety");
		pet1.setPetAge(2);
		pet1.setPetPlace("SweetyVilla");
		pet1.setUser(user);

		petDAO.savePet(pet);

		petDAO.savePet(pet1);

		List<Pet> listOfMyPets = petDAO.getMyPets((int) savedUser.getUserId());

		Assert.assertEquals(pet.getPetName(), listOfMyPets.get(0).getPetName());
		Assert.assertEquals(pet1.getPetName(), listOfMyPets.get(1).getPetName());
		
		user = null;
		pet = null;
		pet1 = null;
		listOfMyPets = null;
	}

	@Test
	public void testTestSavePet() {
		Pet pet = new Pet();
		pet.setPetName("Tommy");
		pet.setPetAge(2);
		pet.setPetPlace("TommyVilla");

		Pet savedPet = petDAO.savePet(pet);

		Assert.assertEquals(pet.getPetName(), savedPet.getPetName());

		Assert.assertNotNull("Pet Id exist", savedPet.getPetId());
		
		pet = null;
		savedPet = null;
	}

	@Test
	public void testTestBuyPet() {
		User owner = new User();
		owner.setUserName("Test3");
		owner.setUserPassword("test33");
		owner = userDAO.saveUser(owner);
		
		Pet pet = new Pet();
		pet.setPetName("Snoffy");
		pet.setPetAge(3);
		pet.setPetPlace("SnoffyVilla");
		pet.setUser(owner);

		Pet savedPet = petDAO.savePet(pet);
		
		Assert.assertNotNull(savedPet.getUser().getUserId());

		User buyer = new User();
		buyer.setUserName("Test4");
		buyer.setUserPassword("test44");
		
		buyer = userDAO.saveUser(buyer);
		Assert.assertNotNull(buyer.getUserId());
		
		Pet myPet = petDAO.buyPet((int) savedPet.getPetId(), (int) buyer.getUserId());
		
		
		Assert.assertEquals(buyer.getUserId(), myPet.getUser().getUserId());
		Assert.assertNotEquals(savedPet.getUser().getUserId(), myPet.getUser().getUserId());
		
		pet = null;
		savedPet = null;
		owner = null;
		buyer = null;
		myPet = null;
	}
}



