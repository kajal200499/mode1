<!-- @author Chandni -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    
    <%@ page errorPage="error.jsp" %> 
    
    <%@page import="com.casestudy.model.User"%>
    <%@page import="com.casestudy.model.Pet"%>
	<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>My Pets</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <style>
table, td, th {
  border: 1px solid black;
}

table {
  width: 100%;
  border-collapse: collapse;
}
.petlist{
color: white;

}
</style>
</head>
<body>
<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">PetPeers</a>
			</div>
			<ul class="nav navbar-nav">
				<li><a href="home">Home</a></li>
				<li class="active"><a href="myPets">My Pet</a></li>
				<li><a href="addPet">Add Pets</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
				 <li><a href="logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
			</ul>
		</div>
	</nav>
	
	

<!-- <h1>Welcome to My Pets Page</h1> -->

<%! 
User user; 
%>

<!-- Debugging Content -->

<%-- <%
	user = user = (User) session.getAttribute("userData");
	 if (user != null) {
		 out.println("User Name : " + user.getUserId());
	 } else {
		 out.println("Oops !! No User Found " );
	 }
%>
<br>
User : ${userData }

${user.getPets()}

<h2>User's Data</h2>

<c:if test="${userData != null}">
	    <tr>
	      <td>User Name : <c:out value="${userData.getUserName()}" /></td>
	      <td>User Id : <c:out value="${userData.getUserId()}" /></td>
	    </tr>
	</c:if> --%>


<!-- MyPets Content -->

<div class="row">
    <div class="container">
    
<table>
	<tr>
		<th class="petlist" bgcolor= "black"  colspan="4"  >Pet List</th>
	</tr>
	
  <tr>
    <th>Pet ID</th>
    <th>Pet Name</th>
     <th>Pet Age</th>
     <th>Pet Place</th>
     </tr>
  <c:forEach items="${listOfPets}" var="pet">
    
	<tr>
		<td><c:out value="${pet.getPetId()}" /></td>
	    <td><c:out value="${pet.getPetName()}" /></td>
	    <td><c:out value="${pet.getPetAge()}" /></td>
	    <td><c:out value="${pet.getPetPlace()}" /></td>
    </tr>
  </c:forEach>
</table>
</div>
</div>
</body>
</html>