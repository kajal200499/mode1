package com.casestudy.test;


/**
 * 
 * @author KAJAL RANI
 *
 */

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.casestudy.model.Pet;
import com.casestudy.model.User;
import com.casestudy.service.PetService;
import com.casestudy.service.UserService;


@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/appConfig-servlet.xml" })
@RunWith(SpringRunner.class)
public class PetServiceTest {

	@Autowired(required = true)
	PetService petService;

	@Autowired
	UserService userService;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetAllPets() {
		// Get All Pets
		User user = new User();
		user.setUserName("Test1");
		user.setUserPassword("test11");

		User savedUser = userService.saveUser(user);

		Pet pet = new Pet();
		pet.setPetName("Happy");
		pet.setPetAge(1);
		pet.setPetPlace("happyVilla");
		pet.setUser(savedUser);

		Pet pet1 = new Pet();

		pet1.setPetName("Sweety");
		pet1.setPetAge(2);
		pet1.setPetPlace("SweetyVilla");

		petService.savePet(pet);

		petService.savePet(pet1);

		List<Pet> listOfAllPets = petService.getAllPets();

		Assert.assertEquals(pet.getPetName(), listOfAllPets.get(0).getPetName());
		Assert.assertEquals(pet1.getPetName(), listOfAllPets.get(1).getPetName());
		
		user = null;
		pet = null;
		pet1 = null;
		listOfAllPets = null;

	}

	@Test
	public void testGetMyPets() {
		User user = new User();
		user.setUserName("Test2");
		user.setUserPassword("test22");

		User savedUser = userService.saveUser(user);

		Pet pet = new Pet();
		pet.setPetName("Happy");
		pet.setPetAge(1);
		pet.setPetPlace("happyVilla");
		pet.setUser(user);

		Pet pet1 = new Pet();

		pet1.setPetName("Sweety");
		pet1.setPetAge(2);
		pet1.setPetPlace("SweetyVilla");
		pet1.setUser(user);

		petService.savePet(pet);

		petService.savePet(pet1);

		List<Pet> listOfMyPets = petService.getMyPets((int) savedUser.getUserId());

		Assert.assertEquals(pet.getPetName(), listOfMyPets.get(0).getPetName());
		Assert.assertEquals(pet1.getPetName(), listOfMyPets.get(1).getPetName());
		
		user = null;
		pet = null;
		pet1 = null;
		listOfMyPets = null;

	}

	@Test
	public void testSavePet() {
		Pet pet = new Pet();
		pet.setPetName("Tommy");
		pet.setPetAge(2);
		pet.setPetPlace("TommyVilla");

		Pet savedPet = petService.savePet(pet);

		Assert.assertEquals(pet.getPetName(), savedPet.getPetName());

		Assert.assertNotNull("Pet Id exist", savedPet.getPetId());
		
		pet = null;
		savedPet = null;

	}

	@Test
	public void testBuyPet() {
		
		User owner = new User();
		owner.setUserName("Test3");
		owner.setUserPassword("test33");
		owner = userService.saveUser(owner);
		
		Pet pet = new Pet();
		pet.setPetName("Snoffy");
		pet.setPetAge(3);
		pet.setPetPlace("SnoffyVilla");
		pet.setUser(owner);

		Pet savedPet = petService.savePet(pet);
		
		Assert.assertNotNull(savedPet.getUser().getUserId());

		User buyer = new User();
		buyer.setUserName("Test4");
		buyer.setUserPassword("test44");
		
		buyer = userService.saveUser(buyer);
		Assert.assertNotNull(buyer.getUserId());
		
		Pet myPet = petService.buyPet((int) savedPet.getPetId(), (int) buyer.getUserId());
		
		
		Assert.assertEquals(buyer.getUserId(), myPet.getUser().getUserId());
		Assert.assertNotEquals(savedPet.getUser().getUserId(), myPet.getUser().getUserId());
		
		pet = null;
		savedPet = null;
		owner = null;
		buyer = null;
		myPet = null;


	}

}
