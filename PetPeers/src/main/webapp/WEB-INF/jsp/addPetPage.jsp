<!-- @author Chandni -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
    <%@ page errorPage="404.jsp" %> 
	
<%@page import="com.casestudy.model.Pet"%>
<%@page import="com.casestudy.model.User"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add Pet</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<style>
.btn {
	padding: 8px 4px;
	margin: 4px 2px;
}
table{
 border-collapse: collapse;
width: 40px;
height: 60px;
}

.petinfo {
	background-color: black;
	color: white;
}

.error {
	color: #ff0000;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">PetPeers</a>
			</div>
			<ul class="nav navbar-nav">
				<li><a href="home">Home</a></li>
				<li><a href="myPets">My Pet</a></li>
				<li class="active"><a href="savePet">Add Pets</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="logout"><span
						class="glyphicon glyphicon-log-out"></span> Logout</a></li>
			</ul>
		</div>
	</nav>

	<!--  Current User Data -->
<%-- 	User Data : ${userData} --%>


	<!--  Add Pet form  -->

	<form:form action="addPet" modelAttribute="pet" method="POST">
		<div class="container-fluid">
			<table border="1" align="center" cellspacing="5px" cellpadding="5%">
				<tr>
					<th class="petinfo" colspan="2">Pet Information</th>
				</tr>
				<tr>
					<td><form:label path="petName">Name:</form:label></td>
					<td>
						<form:input type="text" path="petName" name="petName" />
						<form:errors path="petName" class="error"></form:errors>
					</td>
					
				</tr>
				<tr>
					<td><form:label path="petAge">Age:</form:label></td>
					<td>
						<form:input type="number" path="petAge" name="petAge" min="0"/>
						<form:errors path="petAge" class="error"></form:errors>
					</td>
					
				</tr>
				<tr>
					<td><form:label path="petPlace">Place:</form:label></td>
					<td>
						<form:input type="text" path="petPlace" name="petPlace" />
						<form:errors path="petPlace" class="error"></form:errors>
					</td>
				</tr>
			
				
				<c:if test="${userData != null}">
					<tr>
						<td><form:input type="hidden" path="user.UserId"
								value="${userData.getUserId()}" /></td>
						<td><form:input type="hidden" path="user.userName"
								value="${userData.getUserName()}" /></td>
						<td><form:input type="hidden" path="user.userPassword"
								value="${userData.getUserId()}" /></td>
						<td><form:input type="hidden" path="user.confirmPassword"
								value="${userData.getUserId()}" /></td>
					</tr>
				</c:if>
				
				<tr>
					<td align="center" colspan="2">
					<input type="submit" name="savePet" 
						class="btn btn-primary" value="Add Pet" />
					<input type="reset" name="cancel" 
						class="btn btn-primary" value="Cancel" />
				</tr>

			</table>

		</div>


	</form:form>
</body>
</html>