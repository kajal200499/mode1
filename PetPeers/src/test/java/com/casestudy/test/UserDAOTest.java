package com.casestudy.test;


/**
 * 
 * @author KAJAL RANI
 *
 */


import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.casestudy.dao.UserDAO;
import com.casestudy.model.User;


@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/appConfig-servlet.xml" })
@RunWith(SpringRunner.class)
public class UserDAOTest {
	
	@Autowired
	UserDAO userDAO;
	
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testSaveUser() {
		User user = new User();
		user.setUserName("Test1");
		user.setUserPassword("Test11");
		
		User savedUser = userDAO.saveUser(user);
		
		Assert.assertEquals(user.getUserName(), savedUser.getUserName());
		
		Assert.assertNotNull(savedUser);
		Assert.assertNotNull(savedUser.getUserId());
		
		user = null;
		savedUser = null;
	}

	@Test
	public void testAuthenticateUser() {
		User user = new User();
		user.setUserName("Test2");
		user.setUserPassword("Test22");
		
		User savedUser = userDAO.saveUser(user);
		
		Assert.assertNotNull(savedUser);
		Assert.assertNotNull(savedUser.getUserId());
		
		User user1 = new User();
		user1.setUserName("Test2");
		user1.setUserPassword("Test22");
		
		
		User user2 = new User();
		user2.setUserName("Test4");
		user2.setUserPassword("Test44");
		
		User authUser1 = userDAO.authenticateUser(user1);
		
		User authUser2 = userDAO.authenticateUser(user2);
		
		Assert.assertEquals(user.getUserName(), authUser1.getUserName());
		Assert.assertNull(authUser2);
		
		user = null;
		savedUser = null;
		user1 = null;
		user2 = null;
		authUser1 = null;
		authUser2 = null;
	}

	@Test
	public void testFindByUsername() {
		User user = new User();
		user.setUserName("Test5");
		user.setUserPassword("Test55");
		
		User savedUser = userDAO.saveUser(user);
		
		Assert.assertNotNull(savedUser);
		Assert.assertNotNull(savedUser.getUserId());
		
		String actualName = "Test5";
		String wrongName = "Test6";
		
		User foundUser = userDAO.findByUsername(user.getUserName());
		
		Assert.assertEquals(actualName, foundUser.getUserName());
		
		Assert.assertNotEquals(wrongName, foundUser.getUserName());
		
		user = null;
		savedUser = null;
	}



}
