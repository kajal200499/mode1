package com.casestudy.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;


/**
 * Servlet Filter implementation class MainFilter
 */
@WebFilter(filterName = "MainFilter", urlPatterns = "/*")
public class MainFilter implements Filter {

	private static final Logger logger = Logger.getLogger(MainFilter.class);

	/**
	 * Default constructor.
	 */
	public MainFilter() {
		// Main Constructor
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// Destroy servlet filter
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// Check user login

		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		
		List<String> urls = new ArrayList<>(
	            Arrays.asList("/PetPeers/", "/PetPeers/login", "/PetPeers/register"));
		
		logger.debug(servletRequest.getRequestURI());

		HttpSession httpSession;
		if (! urls.contains(servletRequest.getRequestURI())) {
			if (request.getAttribute("userData") == null) {
				httpSession = ((HttpServletRequest) request).getSession();

				logger.info("Request Attribute does not have user ");

				if (httpSession != null && httpSession.getAttribute("userData") == null) {

					logger.info("Session Attribute does not have user ");
					String encodedRedirectURL = ((HttpServletResponse) response)
							.encodeRedirectURL(servletRequest.getContextPath() + "/login");
					
					logger.info("Redirecting url = " + encodedRedirectURL);

					servletResponse.setStatus(HttpStatus.TEMPORARY_REDIRECT.value());
					servletResponse.setHeader("Location", encodedRedirectURL);
				}
			}

		}
		logger.info("Request Attribute have user = " + request.getAttribute("userData"));
		chain.doFilter(request, response);

	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {

	}

}
