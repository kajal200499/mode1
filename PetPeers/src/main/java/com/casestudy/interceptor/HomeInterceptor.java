package com.casestudy.interceptor;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.casestudy.model.Pet;
import com.casestudy.model.User;
import com.casestudy.service.PetService;



/**
 * 
 * @author KAJAL RANI
 *
 */

public class HomeInterceptor extends HandlerInterceptorAdapter {
    
    private static final Logger logger = Logger.getLogger(HomeInterceptor.class);
    
    @Autowired
    public PetService petService;

    //before the actual handler will be executed
    public boolean preHandle(HttpServletRequest request, 
        HttpServletResponse response, Object handler)
        throws Exception {
    	
    	logger.debug("---PreHandle ----");
    	
    	if (request.getAttribute("userData") == null) {
    		logger.debug(handler);
    		logger.debug(response);    		
    	}
        
        return true;
    }

    //after the handler is executed
    public void postHandle(
        HttpServletRequest request, HttpServletResponse response, 
        Object handler, ModelAndView modelAndView)
        throws Exception {
        
    	
    	logger.debug("---Post Handle ----");
    	logger.debug("---handler ----" + handler);
    	logger.debug("---modelAndView ----" + modelAndView);
    	logger.debug("---request ----" + request);

        
        HttpSession httpSession = request.getSession();
        List<Pet> listOfAllPets = new ArrayList<>();
        
        logger.debug("---userData ----" + (request.getAttribute("userData")));
        logger.debug("---userData ----" + (httpSession.getAttribute("userData")));
        
        
        if (request.getAttribute("userData") != null) {
    		logger.debug("----User attrib ----" + ((User) request.getAttribute("userData")).getUserName());
    		listOfAllPets = petService.getAllPets();
    	} else if(httpSession.getAttribute("userData") != null) {
    		User user = (User) httpSession.getAttribute("userData");
    		request.setAttribute("userData", user);
    		request.setAttribute("userID", user.getUserId());
    		
    		logger.debug("----User attrib ----" + user.getUserName());
    		listOfAllPets = petService.getAllPets();
    	}
        
		logger.info("--- All Pets in Filter ---" + listOfAllPets);
		request.setAttribute("listOfAllPets", listOfAllPets);

    }
}