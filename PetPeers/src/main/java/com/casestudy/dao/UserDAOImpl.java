package com.casestudy.dao;


import java.util.List;


import javax.transaction.Transactional;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import com.casestudy.model.User;

/**
 * 
 * @author KAJAL RANI
 *
 */

@Transactional
@Repository(value = "UserDAO")
public class UserDAOImpl implements UserDAO {

	@Autowired
	private SessionFactory sessionFactory;

	private static final Logger logger = LogManager.getLogger(UserDAOImpl.class);

	@Override
	public User saveUser(User user) {
		// Saves User in Database
		
		logger.info("Dao SessionFactory = " + this.sessionFactory);
		Session session = this.sessionFactory.getCurrentSession();
		logger.info("Dao Session = " + session);
		try {
			logger.debug("----------------Saving the records-----------------------");
			Long userId = (Long) session.save(user);
			user.setUserId(userId);
		} catch (ConstraintViolationException cve) {
			logger.error("------- Constraint Violation in saving User --------" + cve.getMessage());
			cve.getMessage();
			session.flush();
			session.clear();
		}
		
		logger.info("User Saved successfully, User details=" + user);

		return user;

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public User authenticateUser(User user) {
		// Get User from database

		User userData = null;

		Session session = this.sessionFactory.getCurrentSession();

		logger.info("----------------(DAO Layer) Reading the records-----------------------");
		
		String getQuery ="from User where userName = :userName and userPassword = :userPassword ";
		
		Query query = session.createQuery(getQuery);
		query.setParameter("userName", user.getUserName());
		query.setParameter("userPassword", user.getUserPassword());
		
		List<User> users = (List<User>) query.list();
		
		if (users != null) {
			for (User user1 : users) {
				logger.debug(user1.getUserName());
				if (user1.getUserName().equals(user.getUserName()) && user1.getUserPassword().equals(user.getUserPassword())) {
					userData = user1;
					break;
				}
			}
			
		} else  {
			logger.warn("UserData is not correct " + users);
		}
		
		System.out.println("----------------(DAO Layer) Done Authenticate records-----------------------" + userData);
		return userData;

	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public User findByUsername(String userName) {
		// Find User By Name 
		User userData = null;

		Session session = this.sessionFactory.getCurrentSession();

		logger.info("----------------(DAO Layer) Start FindUserByName-----------------------");
		
		String getQuery ="from User where userName = :userName ";
		
		Query query = session.createQuery(getQuery);
		query.setParameter("userName", userName);
		
		List<User> users = (List<User>) query.list();
		
		if (users != null && users.size() > 0) {
			userData = users.get(0);
		} else  {
			logger.warn("No User found with name  " + userName );
		}

		logger.info("----------------(DAO Layer) End FindUserByName-----------------------" + userData);
		return userData;
		
	}
	
	
	// Getters and Setters

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	

}
