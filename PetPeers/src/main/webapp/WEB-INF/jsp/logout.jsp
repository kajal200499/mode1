<!-- @author Chandni -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
	
    <%@ page errorPage="error.jsp" %> 

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>JSP Page</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>
<body>
	<%
	session.removeAttribute("username");
	session.removeAttribute("password");
	session.invalidate();
	response.sendRedirect("loginPage.jsp");
	%>
	<h1>Logout was done successfully</h1>
</body>
</html>
