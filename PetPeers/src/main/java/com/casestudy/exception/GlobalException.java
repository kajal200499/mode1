package com.casestudy.exception;



import java.io.FileNotFoundException;

 

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

 

@ControllerAdvice
public class GlobalException {
	
	
    @ExceptionHandler(value= {UserException.class,PetException.class, FileNotFoundException.class})
    public ModelAndView processException(Exception exception) {
    	// Global Exceptions Handler
        ModelAndView modelAndView=new ModelAndView("globalException");
        modelAndView.addObject("exceptionMsg", exception.getMessage());
        return modelAndView;
    }

 

}