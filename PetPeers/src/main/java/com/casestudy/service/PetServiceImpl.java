package com.casestudy.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.casestudy.dao.PetDAO;
import com.casestudy.model.Pet;

/**
 * 
 * @author KAJAL RANI
 *
 */
@Service(value = "")
@Transactional
public class PetServiceImpl implements PetService {

	@Autowired
	private PetDAO petDAO;

	private static final Logger logger = LogManager.getLogger(PetServiceImpl.class);

	@Override
	public List<Pet> getAllPets() {
		// Get All Pets

		List<Pet> listOfPets = new ArrayList<>();
		logger.info("----------------(Get All Pets) Start Service Layer-----------------------");

		listOfPets = petDAO.getAllPets();
		logger.info("----------------(Get All Pets) Ending the records-----------------------");

		return listOfPets;
	}

	@Override
	public List<Pet> getMyPets(int userId) {
		// Get My Pets from userId

		List<Pet> listOfPets = new ArrayList<>();
		if (userId > 0) {
			logger.info("----------------(Get MyPets) Start Service Layer-----------------------");

			listOfPets = petDAO.getMyPets(userId);
			logger.info("----------------(Get MyPets) Ending the records-----------------------");
		} else {
			logger.warn("----------------Something went wrong in Service Layer (Get My Pets)------------");

		}

		return listOfPets;
	}

	@Override
	public Pet savePet(Pet pet) {
		// Saves User after Validation

		Pet petData = null;
		if (pet.getPetName() != null) {
			logger.info("---------------- (Save) Start Service Layer-----------------------");

			petData = petDAO.savePet(pet);
			logger.info("---------------- (Save) Ending the records-----------------------");
		} else {
			logger.warn("----------------(Save) Something went wrong in Service Layer------------");
		}

		return petData;
	}

	@Override
	public Pet buyPet(int petId, int userId) {
		// Buy Pet

		Pet petData = null;
		if (petId > 0 && userId > 0) {
			logger.info("---------------- (Save) Start Service Layer-----------------------");
			try {
				petData = petDAO.buyPet(petId, userId);
			} catch (Exception e) {
				e.getMessage();
				logger.error("---------------- (Exception in buy pet)-----------------------");
			}

			logger.debug("---------------- (Buy Pet)-----------------------");
		} else {
			logger.warn("----------------(Save) Something went wrong in Service Layer------------");
		}

		return petData;
	}

}
