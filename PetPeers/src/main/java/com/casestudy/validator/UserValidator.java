package com.casestudy.validator;

/**
 * 
 * @author KAJAL RANI
 *
 */

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.casestudy.model.User;
import com.casestudy.service.UserService;

@Component
public class UserValidator implements Validator {

	public static Logger logger = LogManager.getLogger(UserValidator.class);

	@Autowired
	private UserService userService;

	@Override
	public void validate(Object object, Errors errors) {
		// Validates User

		logger.info("--- UserValidator validate() Start ---");

		if (object instanceof User) {
			User user = (User) object;

			logger.info("--- UserValidator validate() Object ---" + user);

			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userName", "NotEmpty.userModel.userName");

			if (!(errors.hasFieldErrors("userPassword"))) {
				if (user.getUserName().length() < 4 || user.getUserName().length() > 16) {
					errors.rejectValue("userName", "Size.userModel.userName");
				} else if (userService.findByUsername(user.getUserName()) != null) {
					errors.rejectValue("userName", "Duplicate.userModel.userName");
				}

			}

			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userPassword", "NotEmpty.userModel.userPassword");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmPassword", "NotEmpty.userModel.confirmPassword");

			if (!((errors.hasFieldErrors("userPassword")) || (errors.hasFieldErrors("confirmPassword")))) {
				if (user.getUserPassword().length() < 6 || user.getUserPassword().length() > 20) {
					errors.rejectValue("userPassword", "Size.userModel.userPassword");
				}

				else if (!user.getConfirmPassword().equals(user.getUserPassword())) {
					errors.rejectValue("confirmPassword", "Diff.userModel.confirmPassword");
				}

			}

		}
		logger.info("--- UserValidator validate() End ---");

	}

	@Override
	public boolean supports(Class<?> clazz) {

		return User.class.equals(clazz);
	}

}
