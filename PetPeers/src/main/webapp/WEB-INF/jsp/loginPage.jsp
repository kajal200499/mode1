<!-- @author Chandni -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
    <%@ page errorPage="error.jsp" %> 


<%@page import="com.casestudy.model.User"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%@page import="com.casestudy.model.User"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login Page</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<style type="text/css">
.error {
	color: ff0000;
	font-size: 20px;
}

#registrationMsg {
	color: #00ff00;
}
</style>

</head>
<body>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Pet Shop</a>
			</div>

			<ul class="nav navbar-nav navbar-right">
				<li><a href="register"><span
						class="glyphicon glyphicon-log-in"></span> Sign-up</a></li>
			</ul>
		</div>
	</nav>

	<!-- User Registration Page  -->

	<%-- 	
	<!--  Print User Data -->
	<%
	User user = (User) request.getAttribute("userData");

	if (user !=null) {
		out.print("User Name " + user.getUserName());
		out.print("User Id : " + user.getUserId());
	} 
%> --%>

	<div class="row">
		<div class="container">
			<div>
				<h1>Login</h1>
			</div>
			<div>
				<!--  Print Registration Message -->
				<h4 id="registrationMsg">${registrationMsg}</h4>
			</div>

		</div>




		<form:form action="login" modelAttribute="user" method="POST">

			<div class="row">
				<div class="container">
					<div>

						<form:label path="userName">Name:</form:label>
						<br>
						<form:input path="userName" name="username" type="text"
							class="form-control" />
						<form:errors path="userName" cssclass="error"></form:errors>
						<br>

						<form:label path="userPassword">Password:</form:label>
						<br>
						<form:input path="userPassword" name="userPassword"
							type="password" class="form-control" />
						<form:errors path="userPassword" cssclass="error"></form:errors>
						<br>

					</div>
					<div>
						<!--  Print Login Message -->
						<h5 id="loginMsg">${loginMsg}</h5>
					</div>

					<input type="submit" class="btn btn-primary" value="Login" />
				</div>
			</div>
		</form:form>
	</div>
</body>
</html>






