package com.pojo;

public class PermanentEmployee extends Employee{
	
	public PermanentEmployee() {
		System.out.println("Permanent Employee Constructor");
	}
	
	public float salary() {
		return 1000;
	}
	

}
