package com.casestudy.service;


import com.casestudy.model.User;

/**
 * 
 * @author KAJAL RANI
 *
 */

public interface UserService {
	
	public abstract User saveUser(User user);
	
	public abstract User authenticateUser(User user);
	
	public abstract User findByUsername(String username);

}
