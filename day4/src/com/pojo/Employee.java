package com.pojo;
/**
 * Main Employee Super Class 
 * @author KAJAL RANI
 *
 */

public class Employee {
	private int empId;
	private String empName;
	private float empSalary;
	private Address var;
	
	public Address getVar() {
		return var;
	}
	
	public void setVar(Address var) {
		this.var = var;
	} 
	
	public Employee() {
		System.out.println("Employee Constructor");
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public float getEmpSalary() {
		return empSalary;
	}

	public void setEmpSalary(float empSalary) {
		this.empSalary = empSalary;
	}

}
