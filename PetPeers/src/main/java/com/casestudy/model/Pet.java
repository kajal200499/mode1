package com.casestudy.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

/**
 * 
 * @author KAJAL RANI
 *
 */

@Entity
@Table(name = "PET")
public class Pet implements Serializable {

	/**
	 * Pet POJO Class
	 */

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "PETID", length = 5)
	@NotNull
	private long petId;

	@Column(name = "PETNAME", length = 55)
	@NotNull
	private String petName;

	@Column(name = "PETAGE", length = 2)
	private Integer petAge;

	@Column(name = "PETPLACE", length = 55)
	private String petPlace;
	
	@Type(type="true_false")
	@Column(name = "ISPETSOLD", length = 55)
	private Boolean isPetSold;

	@ManyToOne
	@JoinColumn(name = "PETOWNERID", nullable = true)
	private User user;

	private static final long serialVersionUID = 1L;

	// Constructors :

	public Pet() {
		super();
	}

	public Pet(long petId, String petName, Integer petAge, String petPlace, User user) {
		super();
		this.petId = petId;
		this.petName = petName;
		this.petAge = petAge;
		this.petPlace = petPlace;
		this.user = user;
	}

	// Getters And Setters :

	public long getPetId() {
		return petId;
	}

	public void setPetId(long petId) {
		this.petId = petId;
	}

	public String getPetName() {
		return petName;
	}

	public void setPetName(String petName) {
		this.petName = petName;
	}

	public Integer getPetAge() {
		return petAge;
	}

	public void setPetAge(Integer petAge) {
		this.petAge = petAge;
	}

	public String getPetPlace() {
		return petPlace;
	}

	public void setPetPlace(String petPlace) {
		this.petPlace = petPlace;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Boolean getIsPetSold() {
		return isPetSold;
	}

	public void setIsPetSold(Boolean isPetSold) {
		this.isPetSold = isPetSold;
	}

	
	
}
