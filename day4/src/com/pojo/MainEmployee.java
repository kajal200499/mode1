package com.pojo;

import com.pojo.Employee;
import com.pojo.PermanentEmployee;

public class MainEmployee {

	public static void main(String[] args) {
		Employee emp1 = new Employee();
		emp1.getEmpId();
		emp1.getEmpName();
		
		PermanentEmployee permEmp1 = new PermanentEmployee();
		permEmp1.getEmpId();
		permEmp1.getEmpName();
		permEmp1.salary();
		
		PermanentEmployee permEmp2 = new PermanentEmployee();
		permEmp2.getEmpId();
		permEmp2.getEmpName();
		permEmp2.salary();
		

	}

}
