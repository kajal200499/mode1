<!-- @author Chandni -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPEhtml>
<html>
<head>
<metacharset="ISO-8859-1">
<title>Registration Page</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

</head>
<body>

	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Pet Shop</a>
			</div>
			
				<ul class="nav navbar-nav navbar-right">
				 <li><a href="login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
			</ul>
		</div>
	</nav>

	<h1>Register</h1>

	<form:form action="saveUser" modelAttribute="user" method="POST">
 
		<label for="userName">Name:</label>
		<br> 
		<form:input path="userName" name="username"
			type="text" class="form-control" />
		<form:errors path="userName" class="error">
		</form:errors>
 
		 <label for="userPassword">Password:</label>
		<br>
		<form:input path="UserPassword" name="userPassword"
			type="text" class="form-control"/>
		<form:errors path="UserPassword" class="error">
		</form:errors>
		
		Confirm Password  : <form:input path="confirmPassword" name="confirmPassword"
			type="password" class="form-control" />
		<form:errors path="confirmPassword" class="error">
		</form:errors>

		<input type ="submit" value="Register" />

	</form:form>


</body>
</html>






