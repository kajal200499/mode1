package com.casestudy.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

/**
 * 
 * @author KAJAL RANI
 *
 */

@Component
@Entity
@Table(name = "USER")
public class User implements Serializable {

	/**
	 * User POJO Class
	 */

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "USERID", length = 5)
	@NotNull
	private long userId;

	@Column(name = "USERNAME", length = 55, unique = true)
	private String userName;

	@Column(name = "USERPASSWORD", length = 55)
	private String userPassword;

	@Transient
	private String confirmPassword;

	@OneToMany(cascade = CascadeType.REMOVE)
	@Column(name = "PETS")
	private Set<Pet> pets;

	private static final long serialVersionUID = 1L;

	// Constructor :

	public User() {
		super();
	}

	public User(long userId, String userName, String userPassword, String confirmPassword, Set<Pet> pets) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.userPassword = userPassword;
		this.confirmPassword = confirmPassword;
		this.pets = pets;
	}

	// Getters and Setters :

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public Set<Pet> getPets() {
		return pets;
	}

	public void setPets(Set<Pet> pets) {
		this.pets = pets;
	}


}
