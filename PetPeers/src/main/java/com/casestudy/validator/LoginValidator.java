package com.casestudy.validator;

/**
 * 
 * @author KAJAL RANI
 *
 */

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import com.casestudy.model.User;
import com.casestudy.service.UserService;;
 
@Component
public class LoginValidator implements Validator {


	public static Logger logger = LogManager.getLogger(UserValidator.class);

	@Autowired
    private UserService userService;

    @Override
    public void validate(Object object, Errors errors) {
    	if (object instanceof User) {
    		User user = (User) object;
    		
    		logger.info("--- LoginValidator validate() Start ---");
    		
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userName", "NotEmpty.userModel.userName");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userPassword", "NotEmpty.userModel.userPassword");
            
            if (!((errors.hasFieldErrors("userPassword")) || (errors.hasFieldErrors("userName")))) {
	            if (userService.authenticateUser(user) == null) {
	            	logger.error("--- LoginValidator validate() > Duplicate User ---");
	                errors.rejectValue("userName", "Invalid.userModel.userName_userPassword");
	            }
	            
            }

            logger.info("--- LoginValidator validate() End ---");


    	}
    	
    }
   
	@Override
	public boolean supports(Class<?> clazz) {
		
		return User.class.equals(clazz);
	}


}

