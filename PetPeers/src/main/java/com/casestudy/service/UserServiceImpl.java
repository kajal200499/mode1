package com.casestudy.service;


import javax.transaction.Transactional;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.casestudy.dao.UserDAO;
import com.casestudy.model.User;

/**
 * 
 * @author KAJAL RANI
 *
 */

@Service(value = "UserService")
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDAO;

	private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);

	@Override
	public User saveUser(User user) {
		// Saves User after Validation

		User userData = null;
		if (user.getUserName() != null && user.getUserPassword() != null) {
			logger.info("---------------- (Save) Start Service Layer-----------------------");

			try {
				userData = userDAO.saveUser(user);
				logger.info("---------------- (Save) Ending the records-----------------------");
			} catch (ConstraintViolationException cve) {
				logger.error("------- Constraint Violation in saving User --------" + cve.getMessage());
				cve.getMessage();

			}
		} else {
			logger.debug("----------------(Save) Something went wrong in Service Layer------------");
		}

		return userData;
	}

	@Override
	public User authenticateUser(User user) {
		// Authenticates User after Validation
		
		User userData = null;
		if (user.getUserName() != null && user.getUserPassword() != null) {
			logger.info("----------------(Authenticate) Start Service Layer-----------------------");
			userData = userDAO.authenticateUser(user);
			logger.info("----------------(Authenticate) Ending the records-----------------------");
		} 
		else {
			logger.debug("----------------Something went wrong in Service Layer (Authenticate)------------");

		}

		logger.info("Return from Service Layer User = " + userData);

		return userData;
	}

	@Override
	public User findByUsername(String username) {
		// Find User with Specific Name
		
		logger.info("----UserService FindByUserName() ---");
		return userDAO.findByUsername(username);
	}

}
