package com.casestudy.exception;

/**
 * 
 * @author KAJAL RANI
 *
 */

public class PetException extends Exception {

	/**
	 * Pet Exception
	 */
	private static final long serialVersionUID = 1L;
	
	private String message;

	public PetException() {
		super();
	}

	public PetException(String message) {
		super(message);
		
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
