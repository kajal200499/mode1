package com.casestudy.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.casestudy.exception.PetException;
import com.casestudy.exception.UserException;
import com.casestudy.model.Pet;
import com.casestudy.model.User;
import com.casestudy.service.UserService;
import com.casestudy.validator.LoginValidator;
import com.casestudy.validator.PetValidator;
import com.casestudy.validator.UserValidator;
import com.casestudy.service.PetService;


/**
 * 
 * @author KAJAL RANI
 *
 */

@Controller
public class MainController {
	
	public static Logger logger = LogManager.getLogger(MainController.class);
	
	@Autowired
	public UserService userService;
	
	@Autowired
	public PetService petService;
	
	@Autowired
    private UserValidator userValidator;
	
	@Autowired
	private LoginValidator loginValidator;
	
	@Autowired
	private PetValidator petValidator;
	
	
	@GetMapping(value={"/"})
	public String init() {
		/** 
		 *Return homePage
		 */	
		
		return "redirect:/register";
		
	}
	
	@GetMapping(value={"/index"})
	public ModelAndView index() {
		/** 
		 *Index Page Method : Redirect to Index page
		 */
		
		logger.info("--- Invoke index() ---");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("index");
		
		return modelAndView;
		
	}
	
	@GetMapping(value={"/home"})
	public ModelAndView home() {
		
		/** 
		 *Home Page Method : Redirect to homePage
		 */
		
		logger.info("--- Invoke home() ---");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("home");
		
		return modelAndView;
		
	}
	
	
	@RequestMapping(value="/login", method = RequestMethod.GET)
	public ModelAndView login() {
		/**
		 *Redirects the User to Login page
		 */
		
		logger.info("--- login() Start  ---");
		ModelAndView modelAndView = new ModelAndView("loginPage");
		
		User user = new User();
		modelAndView.addObject("user", user);
		
		logger.info("--- login() End ---" + user);
		
		return modelAndView;
	
	}
	
	
	@RequestMapping(value="/login", method = RequestMethod.POST)
	public ModelAndView login(HttpServletRequest request, @ModelAttribute("user") User user, BindingResult bindingResult) throws UserException {
		/**
		 *Redirects the User to Login page or Home Page
		 */
		
		logger.info("--- Invoke login() Post ---");
		ModelAndView modelAndView = new ModelAndView("loginPage");
		
		loginValidator.validate(user, bindingResult);

        if (bindingResult.hasErrors()) {
        	user = null;
            return modelAndView;
        } else if (user == null) {
        	throw new UserException("User could not be logged in");
        }else {
        	logger.debug("--- login() Post calling authenticateUser---");
        	return this.authenticateUser(request, user);
        }
        
        
        
	}
	
	
	
	@RequestMapping(value="/authenticateUser", method = RequestMethod.POST)
	public ModelAndView authenticateUser(HttpServletRequest request, @ModelAttribute("user") User user) {
		/**
		 * Authenticates the User
		 */
		ModelAndView modelAndView = new ModelAndView("redirect:/login");
		User userData;
		HttpSession httpSession =  request.getSession();

		logger.info("-------Authenticate User Invoked-------");
		if (user != null) {
			// Go to Service Layer
			logger.info("-------Ready to Authenticate User -------");
			userData = userService.authenticateUser(user);
			
			logger.info(" User = " + userData);
			request.setAttribute("userData", userData);
			httpSession.setAttribute("userData", userData);
			
			if (userData != null) {
				logger.info(" Authenticated UserData = "  + userData.getUserName());
				user = userData;
				modelAndView.setViewName("redirect:home");
				
			}
			else {
				// Return to Login Page if no matching user found
				logger.info("------- No matching user found -------");
				modelAndView.setViewName("loginPage");
			}
			logger.info("------- Authenticated User -------");
		} else {
			// Return to Login Page
			logger.info("------- Something went wrong in authenticating User -------");
			modelAndView = this.login();
			modelAndView.setViewName("redirect:/login");
		}
		
		return modelAndView;
	
	}
	
	
	@RequestMapping(value="/register", method = RequestMethod.GET)
	public ModelAndView register() {
		/**
		 * Returns Registration Page with model User
		 */
		
		ModelAndView modelAndView = new ModelAndView("registrationPage");
		
		User user = new User();
		modelAndView.addObject("user", user);
		
		return modelAndView;
	
	}
	
	
	@RequestMapping(value="/register", method = RequestMethod.POST)
	public ModelAndView register(@ModelAttribute("user") User user,  BindingResult bindingResult) throws UserException {
		/**
		 * Returns Registration Page with model User
		 */
		
		logger.info("--- Invoke saveUser() ---");
		ModelAndView modelAndView = new ModelAndView("registrationPage");
		
		userValidator.validate(user, bindingResult);

        if (bindingResult.hasErrors()) {
        	user = null;
            return modelAndView;
        }  else if (user == null) {
        	throw new UserException("User could not be Registered");
		} 
        else {
        	modelAndView = this.saveUser(user);
        	modelAndView.addObject("registrationMsg", "You are successfully registered. Please login now.");
//        	modelAndView.setViewName("redirect:/login");
        	modelAndView.setViewName("loginPage");
        }
        logger.info("-- Redirect to login ---");
        return modelAndView;
	
	}

	
	@RequestMapping(value="/saveUser", method = RequestMethod.POST  )
	public ModelAndView saveUser(@ModelAttribute("user") User user) {
		/**
		 * If user has no errors , save User and return Success page
		 * Else , return to Error page
		 */
		
		logger.info("--- Invoke saveUser() ---");
		ModelAndView modelAndView = new ModelAndView();
		
		if (user != null) {
			logger.info("Ready to  go to Service Layer");
			userService.saveUser(user);
			modelAndView = this.login();
			
			modelAndView.setViewName("loginPage");
			logger.info(" User Saved Out from  Controller");
			
		} else {
			modelAndView.setViewName("redirect:/registrationPage");
		}
		logger.info("Save User Method  Ended");
		
		return modelAndView;
	
	}
		
	
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public ModelAndView logout(HttpServletRequest request) {
		/**
		 * Invalidate Session and Logout User and return to login page
		 */
		
		HttpSession httpSession = request.getSession();
		request.setAttribute("userData", null);
		request.setAttribute("listOfAllPets", null);
		request.setAttribute("listOfPets", null);
		httpSession.invalidate();
		
		ModelAndView modelAndView = this.login();
		modelAndView.setViewName("redirect:/login");
		
		logger.info("Logging Out");
		return modelAndView;
	
	}
	
	
	
	@RequestMapping(value="/myPets", method = {RequestMethod.GET,RequestMethod.POST })
	public ModelAndView myPets(HttpServletRequest request) throws UserException, PetException {
		/**
		 * Get Pets of Users : 
		 */
		
		List<Pet> listOfPets =  new ArrayList<>();
		
		ModelAndView modelAndView = new ModelAndView("myPetsPage");
		
		HttpSession httpSession =  request.getSession();
		
		User user = (User) httpSession.getAttribute("userData");
		
		
		logger.info("Get Pets of User = " + user);
		if (user != null) {
			listOfPets = petService.getMyPets((int) user.getUserId());
			logger.info("User = " + user.getUserId() );
			logger.info("Pets List = " + listOfPets );
			
			
			Set<Pet> userPets = listOfPets.stream().collect(Collectors.toSet());

			user.setPets(userPets); 
			
		} else {
			logger.info("Something went wrong in Getting user's Pets " );
		}
		
		httpSession.setAttribute("listOfPets", listOfPets);
		modelAndView.addObject("userData", user);
		return modelAndView;
	
	}
	
	
	@RequestMapping(value="/addPet", method = RequestMethod.GET)
	public ModelAndView addPet() {
		/**
		 * Returns Add Pet Page with model Pet
		 */
		logger.info("--- Invoke addPet() ---");
		ModelAndView modelAndView = new ModelAndView("addPetPage");
		
		Pet pet = new Pet();
		modelAndView.addObject("pet", pet);
		
		logger.info("--- End addPet() ---");
		
		return modelAndView;
	
	}
	
	@RequestMapping(value="/addPet", method = RequestMethod.POST)
	public ModelAndView addPet(@ModelAttribute("pet") Pet pet,  BindingResult bindingResult) {
		/**
		 * Returns Add Pet Page with model Pet
		 */
		logger.info("--- Invoke addPet() ---");
		ModelAndView modelAndView = new ModelAndView("addPetPage");
		
		petValidator.validate(pet, bindingResult);

        if (bindingResult.hasErrors()) {
        	pet = null;
            return modelAndView;
        } 

        logger.info("--- End addPet() ---");
        return this.savePet(pet);

        
	
	}
	
	@RequestMapping(value="/savePet", params = "savePet", method = RequestMethod.POST)
	public ModelAndView savePet(@ModelAttribute("pet") Pet pet) { 
		/**
		 * If pet has no errors , save Pet and return Home page
		 * Else , return to Error page
		 */
		
		logger.info("--- Invoke savePet() ---");
		ModelAndView modelAndView = new ModelAndView();
		
	
		
		if (pet != null) {
			logger.info("Ready to  go to Service Layer");
			
			petService.savePet(pet);
			modelAndView = this.home();
			
			modelAndView.setViewName("redirect:/home");
			logger.info(" Pet Saved Out from  Controller");
			
		} else {
			modelAndView.setViewName("savePet");
		}
		
		logger.info("Save User Method  Ended");
		
		return modelAndView;
	}
	
	@RequestMapping(value="/savePet", params = "cancel", method = RequestMethod.POST)
	public String cancelAddPet(HttpServletRequest request) {
	    return "redirect:/home";
	}


	public ModelAndView petList(HttpServletRequest request) {
		/**
		 * Get All Pets : 
		 */
		logger.info("--- Invoke petList() ---");
		List<Pet> listOfAllPets =  new ArrayList<>();
		
		ModelAndView modelAndView = new ModelAndView("redirect:/home");
		
		HttpSession httpSession =  request.getSession();
		
		listOfAllPets = petService.getAllPets();
		logger.info("--- List of All Pets ---" + listOfAllPets);
		
		httpSession.setAttribute("listOfAllPets", listOfAllPets);
		logger.info("--- End petList() ---");
		
		return modelAndView;
	}
	
	
	@RequestMapping(value = "buyPet")
	public ModelAndView buyPet(HttpServletRequest request) throws PetException {
		// Buy Pet and return home page
		Pet petData;
		 ModelAndView modelAndView = new ModelAndView("home");
		 
		 logger.info("--- Start buyPet() ---");
		 
		 int userId = Integer.valueOf(request.getParameter("user"));
		 int petId = Integer.valueOf(request.getParameter("pet"));
		    
		    petData  = petService.buyPet(petId, userId);
		    if (petData != null) {
		    	modelAndView.setViewName("redirect:/myPets");
		    } else {
		    	throw new PetException("Could not Buy Pet. \n Please Try Again !!");
		    }
		    
		    
		    return modelAndView;
		
	}
	   

}
