<!-- @author Chandni -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
    <%@ page errorPage="404.jsp" %> 

<%@ page session="false"%>
<%@page import="com.casestudy.model.User"%>
<%@page import="com.casestudy.model.Pet"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home Page</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<style>
table, td, th {
	border: 1px solid black;
}

table {
	width: 100%;
	border-collapse: collapse;
}

.petlist {
	background-color: black;
	color: white;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">PetPeers</a>
			</div>
			<ul class="nav navbar-nav">
				<li class="active"><a href="home">Home</a></li>
				<li><a href="myPets">My Pet</a></li>
				<li><a href="addPet">Add Pets</a></li>
			</ul>
			              
			
			<ul class="nav navbar-nav navbar-right">
				<li><a href="?lang=en">EN</a></li>
                 <li><a href="?lang=fr">FR</a></li>
                 <li><a href="?lang=ar">AR</a></li>
			
				<li><a href="logout"><span
						class="glyphicon glyphicon-log-out"></span> Logout</a></li>
			</ul>
		</div>
	</nav>



	<!--  Debugging content -->

	<%-- 	<p>${userID}</p> 
<p>${userData}</p>
	<% 
	User user = (User) request.getSession().getAttribute("userData");
 	out.print(user.getUserName());
 	out.print(user.getUserId());
 	%> --%>






	<div class="row">
		<div class="container">

			<h2>
				<fmt:message key="label.welcome" />
			</h2>

			<table>
				<tr>
					<th class="petlist" bgcolor="black" colspan="5"
						style="color: white;">Pet List</th>
				</tr>

				<tr>
					<th>Pet ID</th>
					<th>Pet Name</th>
					<th>Pet Age</th>
					<th>Pet Place</th>
					<th>Buy</th>
				</tr>
				<c:if test="${listOfAllPets.size() > 0}">
					<c:forEach items="${listOfAllPets}" var="pet">
						<tr>
							<td><c:out value="${pet.getPetId()}" /></td>
							<td><c:out value="${pet.getPetName()}" /></td>
							<td><c:out value="${pet.getPetAge()}" /></td>
							<td><c:out value="${pet.getPetPlace()}" /></td>

							<c:set var="petOwner" value="${pet.getUser().getUserId()}"></c:set>
							<%-- 	<c:out value="${petOwner}"></c:out>	 --%>
							<c:choose>

								<c:when test="${petOwner eq userID}">
									<td><c:out value="NA" /></td>
								</c:when>

								<c:when test="${pet.getIsPetSold() != null}">
									<td><c:out value="Sold Out" /></td>
								</c:when>
								<c:otherwise>
									<td><a
										href="<c:url value="buyPet">
											<c:param name="user" value="${userID}" />
											<c:param name="pet" value="${pet.getPetId()}" />
										</c:url>">Buy</a></td>
								</c:otherwise>
							</c:choose>

						</tr>
					</c:forEach>
				</c:if>
			</table>

		</div>
	</div>
</body>
</html>