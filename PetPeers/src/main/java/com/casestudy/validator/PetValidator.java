package com.casestudy.validator;

/**
 * 
 * @author KAJAL RANI
 *
 */

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import com.casestudy.model.Pet;

@Component
public class PetValidator implements Validator {

	public static Logger logger = LogManager.getLogger(UserValidator.class);

	@Override
	public void validate(Object object, Errors errors) {
		// Validates User

		logger.info("--- PetValidator validate() Start ---");

		if (object instanceof Pet) {
			Pet pet = (Pet) object;

			logger.info("--- PetValidator validate() Object ---" + pet);

			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "petName", "NotEmpty.petModel.petName");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "petPlace", "NotEmpty.petModel.petPlace");

			if (!errors.hasFieldErrors("petName")) {
				if (pet.getPetName().length() < 3 || pet.getPetName().length() > 12) {
					errors.rejectValue("petName", "Size.petModel.petName");
				}
			}
			if (pet.getPetAge() == null) {
				errors.rejectValue("petAge", "NotNull.petModel.petAge");
			}
			else if (pet.getPetAge() < 0 || pet.getPetAge() >99) {
					errors.rejectValue("petAge", "Size.petModel.petAge");			
			}

		}
		logger.info("--- PetValidator validate() End ---");

	}

	@Override
	public boolean supports(Class<?> petClass) {

		return Pet.class.equals(petClass);
	}

}
