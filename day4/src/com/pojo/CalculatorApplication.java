package com.pojo;

/**
 * 
 * @author KAJAL RANI
 *
 */
public class CalculatorApplication {

	public static void main(String[] args) {
		Calculator calc = new Calculator();
		System.out.println(calc.add(4, 5));
		
		ScientificCalculator scientificCalc = new ScientificCalculator();
		System.out.println(scientificCalc.sin(3));
		System.out.println(scientificCalc.add(2, 3));

	}

}
