package com.casestudy.dao;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.casestudy.model.Pet;

/**
 * 
 * @author KAJAL RANI
 *
 */

@Transactional
@Repository(value="PetDAO")
public class PetDAOImpl implements PetDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	private static final Logger logger = LogManager.getLogger(UserDAOImpl.class);

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<Pet> getAllPets() {
		// Get List Of ALl Pets
		

		List<Pet> listOfPets = new ArrayList<>();
		Session session = this.sessionFactory.getCurrentSession();

		logger.info("----------------(DAO Layer) Getting All Pets-----------------------");
		
		String getQuery ="from Pet " ;
		Query query = session.createQuery(getQuery);
		
		listOfPets = query.list();
		
		if (listOfPets != null) {
			logger.warn("All Pets " + listOfPets.size() );
		}
		else  {
			logger.warn("Could not fetch all pets " + listOfPets );
		}

		logger.info("----------------(DAO Layer) Done Get All Pets-----------------------" + listOfPets);
		
		return listOfPets;
		
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<Pet> getMyPets(int userId) {
		// Get My pets from userId
		
		List<Pet> listOfPets = new ArrayList<>();
		Session session = this.sessionFactory.getCurrentSession();

		logger.info("----------------(DAO Layer) Getting the pets-----------------------");
		
		String getQuery ="FROM Pet WHERE PETOWNERID = :userId " ;
		try {
			Query query = session.createQuery(getQuery);
			query.setParameter("userId", userId);
			
			listOfPets = query.list();
		} catch (Exception e) {
			logger.error("No Pets found for this User " + listOfPets.size() );
		} 
		
		
		if (listOfPets != null) {
			logger.warn("Pets of User " + listOfPets.size() );
		}
		else  {
			logger.warn("UserData is not correct " + listOfPets );
		}

		
		logger.info("----------------(DAO Layer) Done Get Pets-----------------------" + listOfPets);
		return listOfPets;
	
	}

	@Override
	public Pet savePet(Pet pet) {
		// Saves Pet in Database
		
		Pet petData;
		logger.info("Dao SessionFactory = " + this.sessionFactory);
		
		Session session = this.sessionFactory.getCurrentSession();
		logger.info("Dao Session = " + session);

		logger.info("----------------Saving the records-----------------------");
		try {
			long petId = (long) session.save(pet);
			petData = pet;
			pet.setPetId(petId);
			logger.info("Pet Added successfully, Pet details=" + petData);
		} 
		catch (Exception e) {		
			petData = null;
			logger.error("Pet is not added , some error occured =" + petData);
		} finally {
			session.flush();
			logger.debug("Out from savePet" );
		}
		return petData;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Pet buyPet(int petId, int userId) {
		// Buy Pet :
		
		Pet petData = null;
		logger.info("Dao SessionFactory = " + this.sessionFactory);
		
		Session session = this.sessionFactory.getCurrentSession();
		logger.info("Dao Session = " + session);
		
		logger.info("-------Buy Pet----------");
		String updateQuery ="UPDATE Pet SET PETOWNERID = :userId , ISPETSOLD = :isPetSold WHERE PETID = : petId" ;
		try {
			Query query = session.createQuery(updateQuery);
			query.setParameter("userId", userId);
			query.setParameter("isPetSold", 1);
			query.setParameter("petId", petId);
			
			logger.info("query=" + query);
			
			int result = query.executeUpdate();
			
			logger.info("query result = " + result);
			
			
			String getQuery ="FROM Pet WHERE PETID = : petId " ;
			Query petQuery = session.createQuery(getQuery);
			petQuery.setParameter("petId", petId);
			
			petData = (Pet) petQuery.uniqueResult();
			
			logger.info("buyPet=" + petId);
			
		} catch (Exception e) {
			logger.error("Buy Pet Exception Occured" + e.getMessage() );
			logger.error("Buy Pet Exception Occured" + petId );
		} 
		finally {
			session.flush();
			logger.debug("Out from buyPet=" + petId);
		}
		
		return petData;
	}

}
