package com.casestudy.exception;

/**
 * 
 * @author KAJAL RANI
 *
 */

public class UserException extends Exception {

	/**
	 *  User Exception
	 */
	
	private static final long serialVersionUID = 1L;
	
	private String message;

	public UserException() {
		super();
	}

	public UserException(String message) {
		super(message);
		
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


}
