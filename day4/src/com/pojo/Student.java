package com.pojo;

/**
 * Student POJO Class
 * @author KAJAL RANI
 *
 */
public class Student {

	private int studNo;
	private String studentName;
	private int studentAge;

	public Student() {
		super();
	}
	
	public Student(Student stud) {
		this.studNo = stud.studNo;
		this.studentName = stud.studentName;
		this.studentAge = stud.studentAge;
		}

	public Student(int studNo, String studentName, int studentAge) {
		super();
		this.studNo = studNo;
		this.studentName = studentName;
		this.studentAge = studentAge;
	}

	public int getStudNo() {
		return studNo;
	}

	public void setStudNo(int studNo) {
		this.studNo = studNo;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public int getStudentAge() {
		return studentAge;
	}

	public void setStudentAge(int studentAge) {
		this.studentAge = studentAge;
	}

}
